# Работа с API

Получить ответ в виде массива:
------------


```php
$client = new  \Carriers\Iml\Client($login, $password, $is_battle);
```
Cформируем запрос для получения данных:
```php
$query = new \Carriers\Iml\Model\Query\GetStatusesQuery();
$query->setCustomerOrder('2695147');
```

выполним запрос, в ответе получим массив
```php
$client->getOrderApi()->getStatuses($query);
```
Например:
```
[
    {
        "Number": "2695147",
        "State": 2,
        "OrderStatus": 0,
        "StateDescription": "У Курьера",
        "OrderStatusDescription": "-",
        "StatusDate": "2017-10-04T13:22:36.12",
        "ReturnPayment": false,
        "BarCode": "7500426819017",
        "ReturnStatus": 0,
        "CashReceiptAmount": 0,
        "Details": [
            {
                "change": "0001-01-01T00:00:00",
                "productNo": "321424",
                "productName": "Мольберт Nika двусторонний в ассортименте",
                "service": "",
                "amount": 1699,
                "cashRecieptAmount": 0,
                "cancel": "",
                "returnCode": "",
                "productVariant": "",
                "statisticalValueLine": 0
            }
        ],
        "DeliveryDate": "2017-10-04T00:00:00"
    }
]
```

Получить ответ в виде объекта:
------------

```php
$client = new Carriers\Iml($user, $password, $is_battle);
$factory = new OrderStatusRepository($client);
&
/** @var Carriers\Iml\Model\OrderStatus */
$statusOrder = $factory->getStatusByOrderId('11111');
&
/** @var int */
$status = $statusOrder->getOrderStatus();

```
