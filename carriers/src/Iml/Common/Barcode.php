<?php

namespace Carriers\Iml\Common;

/**
 * Класс для работы со трихкодами
 * Генерируются штрихкоды в формате EAN-13
 *
 *
 * Class Barcode
 * @package Carriers\Iml\Common
 */
class Barcode
{

    /** @var  int Номер штрихкода */
    private $number;

    /** @var Barcodegen  */
    private $barcode;

    public function __construct($number)
    {
        $this->number = $number;

        $this->barcode = new Barcodegen($number, 4);
    }


    /**
     * Сохранить изображение штрихкода в файл
     *
     * @param $path
     *
     * @return bool
     */
    public function saveToFile($path)
    {
        return imagepng($this->barcode->image(), $path);
    }

    /**
     * Вывести в браузер
     */
    public function display()
    {
        if (!empty($this->barcode)) {
            return $this->barcode->display();
        }
        return false;
    }


}