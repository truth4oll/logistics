<?php

namespace Carriers\Iml\Api;

use Carriers\Iml\Client;

/**
 * Методы реализующие запросы к API для получения услуг ТК
 *
 * Class Service
 * @package Carriers\Iml\Api
 */
class Service extends AbstractApi
{

    /**
     * Получить список услуг
     *
     * @return mixed
     * @throws \Exception
     */
    public function getServiceList()
    {
        $parameters['query'] = ['type' => 'json'];

        return $this->get(Client::SERVICE_LIST_URI . '/service', $parameters);
    }
}