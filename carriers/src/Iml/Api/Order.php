<?php

namespace Carriers\Iml\Api;

use Carriers\Iml\Client;
use Carriers\Iml\Model\Query\AbstractQuery;


/**
 *
 * Методы реализующие запросы к апи заказов
 *
 * Class Order
 * @package Carriers\Iml\Api
 */
class Order extends AbstractApi
{

    /**
     * Доставка предоплаченого заказа
     */
    const SERVICE_COURIER_PAYED = '24';

    /**
     * Доставка с кассовым обслуживанием
     */
    const SERVICE_COURIER_NOT_PAYED = '24КО';

    /**
     * Доставка Почтой России
     */
    const SERVICE_POSTAL = 'ПОЧТА';

    /**
     * Доставка на ПВЗ предоплаченного заказа
     */
    const SERVICE_PICKUP_PAYED = 'С24';

    /**
     * Доставка на ПВЗ заказа с касс. обсл-ем
     */
    const SERVICE_PICKUP_NOT_PAYED = 'С24КО';


    /**
     * Запрос позволяет создать заказ.
     *
     * @param AbstractQuery $query
     *
     * @return mixed
     * @throws \Exception
     */
    public function createOrder(AbstractQuery $query)
    {
        $parameters['body'] = $query->toArray();

        return $this->post(Client::SERVICE_API_URI . '/Json/CreateOrder', $parameters);
    }

    /**
     * Запрос получает статус и состояние заказа(ов).
     *
     * @param AbstractQuery $query
     *
     * @return mixed
     * @throws \Exception
     */
    public function getStatuses(AbstractQuery $query)
    {
        $parameters['body'] = $query->toArray();

        return $this->post(Client::SERVICE_API_URI . '/Json/GetStatuses', $parameters);
    }

    /**
     * Запрос получает список заказов по критериям
     *
     * @param AbstractQuery $query
     *
     * @return mixed
     * @throws \Exception
     */
    public function getOrders(AbstractQuery $query){
        $parameters['body'] = $query->toArray();

        return $this->post(Client::SERVICE_API_URI . '/Json/GetOrders', $parameters);
    }
}