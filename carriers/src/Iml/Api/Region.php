<?php

namespace Carriers\Iml\Api;

use Carriers\Iml\Client;

/**
 *
 * Методы реализующие запросы к API для получения списка регионов
 *
 * Class Price
 * @package Carriers\Iml\Api
 */
class Region extends AbstractApi
{

    public function getRegionList()
    {
        $parameters['query'] = ['type' => 'json'];

        return $this->get(Client::SERVICE_LIST_URI . '/region', $parameters);
    }
}