<?php

namespace Carriers\Iml\Api;

use Carriers\Iml\Client;


/**
 *
 * Методы реализующие запросы к API для получения стоимости доставки
 *
 * Class Price
 * @package Carriers\Iml\Api
 */
class Price extends AbstractApi
{
    /**
     * Запрос можно сформировать с помощью:
     * @see \Carriers\Iml\Model\Query\GetPriceQuery;
     *
     * Получить стоимость доставки для заказа
     *
     * @param array $data
     *
     * @return mixed
     * @throws \Exception
     */
    public function getPrice(array $data = [])
    {
        $parameters['query'] = (array)$data;

        return $this->get(Client::SERVICE_API_URI . '/GetPrice', $parameters);
    }

}