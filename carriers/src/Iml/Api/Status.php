<?php

namespace Carriers\Iml\Api;

use Carriers\Iml\Client;

/**
 *
 * Методы реализующие запросы к API для получения списка статусов
 *
 * Class Price
 * @package Carriers\Iml\Api
 */
class Status extends AbstractApi
{

    /**
     * Получить все статусы из IML
     *
     * @return mixed
     * @throws \Exception
     */
    public function getStatuses()
    {
        $parameters['query'] = ['type' => 'json'];

        return $this->get(Client::SERVICE_LIST_URI . '/Status', $parameters);
    }

}