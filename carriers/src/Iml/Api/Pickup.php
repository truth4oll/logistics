<?php

namespace Carriers\Iml\Api;

use Carriers\Iml\Client;

/**
 * Методы реализующие запросы к апи ПВЗ
 *
 * Class Pickup
 * @package Carriers\Iml\Api
 */
class Pickup extends AbstractApi
{
    /**
     * Получить список ПВЗ по городу, либо все
     *
     * @param null|int|string $city
     *
     * @return mixed
     * @throws \Exception
     */
    public function getPickupList($city = null)
    {
        $parameters = [];
        if (!empty($city)) {
            $parameters['query']['city'] = $city;
        }

        $content = $this->get(Client::SERVICE_API_URI . '/list/sd', $parameters);

        return $content;
    }

}