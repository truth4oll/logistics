<?php

namespace Carriers\Iml\Api;

use Carriers\Iml\Client;


/**
 * Сделаем запрос, получим ответ и переведем его в массив из json строки
 *
 * Class AbstractApi
 * @package Carriers\Iml\Api
 */
abstract class AbstractApi
{

    /**
     * Сконфигурированный объект клиента
     *
     * @var \Carriers\Iml\Client
     */
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }


    /**
     * @param       $path
     * @param array $parameters
     *
     * @return mixed
     * @throws \Exception
     */
    public function get($path, array $parameters = [])
    {
        $response = $this->getClient()->getHttpClient()->get($path, $parameters);

        return $this->decodeResponse($response);
    }

    /**
     * @param       $path
     * @param array $parameters
     *
     * @return mixed
     * @throws \Exception
     */
    public function post($path, array $parameters = [])
    {
        $response = $this->getClient()->getHttpClient()->post($path, $parameters);

        return $this->decodeResponse($response);
    }

    /**
     * Retrieve the client
     *
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Decode the response
     *
     * @param $response
     *
     * @return mixed
     * @throws \Exception
     */
    private function decodeResponse($response)
    {
        $response = is_string($response) ? json_decode($response, true) : $response;

        if ($this->validResponse($response)) {
            return $response;
        }

        return false;
    }

    /**
     * @param $response
     *
     * @return bool
     * @throws \Exception
     */
    private function validResponse($response)
    {
        if (is_array($response) && !empty($response['Result']) && $response['Result'] == 'Error') {

            $errors = [];
            foreach ($response['Errors'] as $key=>$error) {
                $errors[] = $key.': '.$error['Message'];
            }

            $errorMessage = implode(' || ', $errors);

            throw new \Exception($errorMessage);
        }

        return true;
    }


}