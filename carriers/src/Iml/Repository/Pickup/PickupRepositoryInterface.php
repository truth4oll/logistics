<?php

namespace Carriers\Iml\Repository\Pickup;

/**
 * Interface PickupRepositoryInterface
 * @package Carriers\Iml\Repository\Pickup
 */
interface PickupRepositoryInterface
{
    public function getCollection();

}