<?php


namespace Carriers\Iml\Repository\Pickup;

use Carriers\Iml\Repository\AbstractRepository;
use Carriers\Iml\Factory\PickupFactory;

/**
 * Class PickupRepository
 * @package Carriers\Iml\Repository\Pickup
 */
class PickupRepository extends AbstractRepository implements PickupRepositoryInterface
{

    /**
     * @return \Carriers\Iml\Model\Collection\Pickups
     */
    public function getCollection()
    {
        return $this->getFactory()->createCollection(
            $this->getApi()->getPickupList()
        );
    }

    /**
     * @return \Carriers\Iml\Api\Pickup
     */
    public function getApi()
    {
        return $this->getClient()->getPickupApi();
    }

    /**
     * @return PickupFactory
     */
    public function getFactory()
    {
        return new PickupFactory();
    }
}