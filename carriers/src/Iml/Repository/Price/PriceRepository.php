<?php


namespace Carriers\Iml\Repository\Price;

use Carriers\Iml\Factory\PriceFactory;
use Carriers\Iml\Model\Query\GetPriceQuery;
use Carriers\Iml\Repository\AbstractRepository;

/**
 * Class PriceRepository
 * @package Carriers\Iml\Repository\Price
 */
class PriceRepository extends AbstractRepository
{

    protected $model;

    /**
     * @param GetPriceQuery $query
     *
     * @return \Carriers\Iml\Model\AbstractModel
     */
    public function getPrice(GetPriceQuery $query)
    {
        $query = $query->toArray();

        return $this->getFactory()->create(
            $this->getApi()->getPrice($query)
        );
    }


    /**
     * @return \Carriers\Iml\Api\Price
     */
    public function getApi()
    {
        return $this->getClient()->getPriceApi();
    }


    /**
     * @return PriceFactory
     */
    public function getFactory()
    {
        return new PriceFactory();
    }
}