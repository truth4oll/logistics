<?php


namespace Carriers\Iml\Repository\Service;

use Carriers\Iml\Factory\ServiceFactory;
use Carriers\Iml\Repository\AbstractRepository;

/**
 * Class PriceRepository
 * @package Carriers\Iml\Repository\Price
 */
class ServiceRepository extends AbstractRepository
{

    /**
     * @var
     */
    protected $model;

    /**
     * @return array
     */
    public function getServices()
    {
        return $this->getFactory()->createCollection(
            $this->getApi()->getServiceList()
        );
    }


    /**
     * @return \Carriers\Iml\Api\Service
     */
    public function getApi()
    {
        return $this->getClient()->getServiceApi();
    }


    /**
     * @return ServiceFactory
     */
    public function getFactory()
    {
        return new ServiceFactory();
    }
}