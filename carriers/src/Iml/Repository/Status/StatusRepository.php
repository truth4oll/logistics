<?php


namespace Carriers\Iml\Repository\Status;

use Carriers\Iml\Factory\StatusFactory;
use Carriers\Iml\Repository\AbstractRepository;

/**
 * Class PriceRepository
 * @package Carriers\Iml\Repository\Price
 */
class StatusRepository extends AbstractRepository
{

    /**
     * @var
     */
    protected $model;

    /**
     * @return array
     */
    public function getStatuses()
    {
        return $this->getFactory()->createCollection(
            $this->getApi()->getStatuses()
        );
    }


    /**
     * @return \Carriers\Iml\Api\Status
     */
    public function getApi()
    {
        return $this->getClient()->getStatusesApi();
    }


    /**
     * @return StatusFactory
     */
    public function getFactory()
    {
        return new StatusFactory();
    }
}