<?php

namespace Carriers\Iml\Repository\Order;

use Carriers\Iml\Factory\OrderStatusFactory;
use Carriers\Iml\Model\OrderStatus;
use Carriers\Iml\Model\Query\GetStatusesQuery;
use Carriers\Iml\Repository\AbstractRepository;



/**
 * Class OrderRepository
 * @package Carriers\Iml\Repository\Order
 */
class OrderStatusRepository extends AbstractRepository
{

    /**
     * @param $order_id
     *
     * @return OrderStatus
     * @throws \Exception
     */
    public function getStatusByOrderId($order_id)
    {
        $query = new GetStatusesQuery();
        $query->setCustomerOrder($order_id);

        $result = $this->getApi()->getStatuses($query);

        if (!empty($result[0]) && $orderStatusItem = $result[0]){
            return $this->getFactory()->create(
                $orderStatusItem
            );
        }

        throw new \Exception('Получен пустой ответ по запросу статуса заказа');
    }


    /**
     * @return \Carriers\Iml\Api\Order
     */
    public function getApi()
    {
        return $this->getClient()->getOrderApi();
    }


    /**
     * @return OrderStatusFactory
     */
    public function getFactory()
    {
        return new OrderStatusFactory();
    }


}