<?php

namespace Carriers\Iml\Repository\Order;

use Carriers\Iml\Factory\OrderFactory;
use Carriers\Iml\Model\Order;
use Carriers\Iml\Model\Query\GetOrdersQuery;
use Carriers\Iml\Repository\AbstractRepository;

/**
 * Class OrderRepository
 * @package Carriers\Iml\Repository\Order
 */
class OrderRepository extends AbstractRepository
{

    /**
     * @return \Carriers\Iml\Api\Order
     */
    public function getApi()
    {
        return $this->getClient()->getOrderApi();
    }

    /**
     * @return OrderFactory
     */
    public function getFactory()
    {
        return new OrderFactory();
    }

    /**
     * @param $order_id
     *
     * @return Order
     * @throws \Exception
     */
    public function getStatusByOrderId($order_id)
    {
        $query = new GetOrdersQuery();
        $query->setCustomerOrder($order_id);

        $result = $this->getApi()->getOrders($query);

        if (!empty($result[0]) && $orderItem = $result[0]) {
            return $this->getFactory()->create(
                $orderItem
            );
        }

        throw new \Exception('Получен пустой ответ по запросу статуса заказа');
    }


}