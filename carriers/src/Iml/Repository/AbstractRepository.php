<?php

namespace Carriers\Iml\Repository;


use Carriers\Iml\Api\AbstractApi;
use Carriers\Iml\Client;
use Carriers\Iml\Factory\AbstractFactory;

/**
 * Class AbstractRepository
 * @package Carriers\Iml\Repository
 */
abstract class AbstractRepository
{
    protected $client = null;

    protected $api = null;

    /**
     * Constructor
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Return the client
     *
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Return the API Class
     *
     * @return AbstractApi
     */
    abstract public function getApi();

    /**
     * Return the Factory Class
     *
     * @return AbstractFactory
     */
    abstract public function getFactory();


}