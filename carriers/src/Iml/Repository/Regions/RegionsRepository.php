<?php


namespace Carriers\Iml\Repository\Regions;

use Carriers\Iml\Factory\RegionsFactory;
use Carriers\Iml\Repository\AbstractRepository;

/**
 * Class PriceRepository
 * @package Carriers\Iml\Repository\Price
 */
class RegionsRepository extends AbstractRepository
{

    /**
     * @var
     */
    protected $model;

    /**
     * @return array
     */
    public function getRegions()
    {
        return $this->getFactory()->createCollection(
            $this->getApi()->getStatuses()
        );
    }


    /**
     * @return \Carriers\Iml\Api\Status
     */
    public function getApi()
    {
        return $this->getClient()->getStatusesApi();
    }


    /**
     * @return RegionsFactory
     */
    public function getFactory()
    {
        return new RegionsFactory();
    }
}