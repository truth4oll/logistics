<?php

namespace Carriers\Iml\Http;


use Carriers\Iml\Client;


/**
 * Class HttpClient
 * @package Carriers\Iml\Http
 */
class HttpClient
{
    const HTTP_POST = 'POST';

    const HTTP_GET = 'GET';


    /** @var  array */
    protected $options;

    /**
     * HttpClient constructor.
     *
     * @param $options
     */
    public function __construct($options)
    {
        $this->options = $options;
    }

    /**
     * @param       $uri
     * @param array $parameters
     *
     * @return string
     */
    public function get($uri, array $parameters = [])
    {
        return $this->send($uri, self::HTTP_GET, $parameters);
    }

    /**
     * @param       $uri
     * @param array $parameters
     *
     * @return string
     */
    public function post($uri, array $parameters = [])
    {
        return $this->send($uri, self::HTTP_POST, $parameters);
    }


    /**
     * @param       $uri
     * @param       $method
     * @param array $parameters
     *
     * @return string
     */
    public function send($uri, $method, array $parameters = [])
    {
        $client = new \GuzzleHttp\Client();

        $options = [];

        //merge options
        foreach ($parameters as $key => $parameter) {
            if (is_array($parameter)) {
                foreach ($parameter as $item_key => $parameter_item) {
                    $options[$key][$item_key] = $parameter_item;
                }
            } else {
                $options[$key] = $parameter;
            }
        }
        foreach ($this->getOptions() as $key => $parameter) {
            if (is_array($parameter)) {
                foreach ($parameter as $item_key => $parameter_item) {
                    $options[$key][$item_key] = $parameter_item;
                }
            } else {
                $options[$key] = $parameter;
            }
        }

        Client::$lastRequest = json_encode([$method, $uri, $options]);

        $request = $client->createRequest($method, $uri, $options);

        $response = $client->send($request);
        $content  = $response->getBody()->getContents();

        Client::$lastStatus   = $response->getStatusCode();
        Client::$lastResponse = $content;

        return $content;
    }


    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }


}