<?php

namespace Carriers\Iml\Factory;

use Carriers\Iml\Model\Region;


/**
 * Создаем DTO регионп из полученного ответа API
 *
 * Class RegionsFactory
 * @package Carriers\Iml\Factory
 */
class RegionsFactory extends AbstractFactory
{
    /**
     * @param array $data
     *
     * @return \Carriers\Iml\Model\AbstractModel
     */
    public function create(array $data = [])
    {
        return $this->hydrate(new Region(), $data);
    }
}
