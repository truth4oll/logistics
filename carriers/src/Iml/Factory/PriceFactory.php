<?php

namespace Carriers\Iml\Factory;

use Carriers\Iml\Model\Price;

/**
 * Создаем DTO цены заказа из полученного ответа API
 *
 * Class PriceFactory
 * @package Carriers\Iml\Factory
 */
class PriceFactory extends AbstractFactory
{
    /**
     * Создадим экземпляр класса цены
     *
     * @param array $data
     *
     * @return \Carriers\Iml\Model\AbstractModel
     */
    public function create(array $data = [])
    {
        return $this->hydrate(new Price(), $data);
    }
}
