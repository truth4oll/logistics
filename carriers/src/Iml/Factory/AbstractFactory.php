<?php

namespace Carriers\Iml\Factory;

use Carriers\Iml\Model\AbstractModel;

/**
 * Создание объектов, коллекций объектов из массивов полученных через API
 *
 *
 * Class AbstractFactory
 * @package Carriers\Iml\Factory
 */
abstract class AbstractFactory
{

    /**
     * @param array $data
     *
     * @return mixed
     */
    abstract public function create(array $data = []);

    /**
     * @param array $data
     *
     * @return array
     */
    public function createCollection(array $data = [])
    {
        $collection = [];
        foreach ($data as $item) {
            $collection[] = $this->create($item);
        }

        return $collection;
    }

    /**
     * IML hydrator
     * Наолняет модель данными через метод set...
     *
     * @param AbstractModel $model
     * @param array         $data
     *
     * @return AbstractModel
     */
    protected function hydrate(AbstractModel $model, $data = [])
    {
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $method = 'set' . $key;
                if (method_exists($model, $method)) {
                    $model->{$method}($value);
                }
            }
        }

        return $model;
    }
}