<?php

namespace Carriers\Iml\Factory;

use Carriers\Iml\Model\Collection\Pickups;
use Carriers\Iml\Model\Pickup;


/**
 * * Создаем DTO списка ПВЗ
 *
 * Class PickupFactory
 * @package Carriers\Iml\Factory
 */
class PickupFactory extends AbstractFactory
{

    /**
     * Создадим экземпляр ПВЗ
     *
     * @param array $data
     *
     * @return \Carriers\Iml\Model\AbstractModel
     */
    public function create(array $data = [])
    {
        return $this->hydrate(new Pickup(), $data);
    }

    /**
     * Создадим коллекцию объектов ПВЗ
     *
     * @param array $data
     *
     * @return Pickups
     */
    public function createCollection(array $data = [])
    {
        $collection = new Pickups();
        foreach ($data as $item) {
            $collection->addPickup($this->create($item));
        }

        return $collection;
    }
}
