<?php

namespace Carriers\Iml\Factory;


use Carriers\Iml\Model\Order;
use Carriers\Iml\Model\OrderItem;


/**
 * Создание DTO заказа c позициями полученного из API
 *
 * Class OrderFactory
 * @package Carriers\Iml\Factory
 */
class OrderFactory extends AbstractFactory
{
    /**
     * Создадим заполненный экземпляр класса заказа
     *
     * @param array $data
     *
     * @return Order
     */
    public function create(array $data = [])
    {
        $orderItems = [];
        if (!empty($data['Order']['GoodItems'])) {
            foreach ($data['Order']['GoodItems'] as $detail_item) {
                $orderItems[] = $this->hydrate(new OrderItem(), $detail_item);
            }
            unset($data['GoodItems']);
        }

        /** @var Order $model */
        $model = $this->hydrate(new Order(), $data['Order']);
        $model->setOrderItems($orderItems);

        return $model;
    }
}
