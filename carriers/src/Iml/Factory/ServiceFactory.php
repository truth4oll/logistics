<?php

namespace Carriers\Iml\Factory;


use Carriers\Iml\Model\Service;


/**
 * Услуги предоставляемые IML
 *
 * Class ServiceFactory
 * @package Carriers\Iml\Factory
 */
class ServiceFactory extends AbstractFactory
{
    /**
     * @param array $data
     *
     * @return \Carriers\Iml\Model\AbstractModel
     */
    public function create(array $data = [])
    {
        return $this->hydrate(new Service(), $data);
    }
}
