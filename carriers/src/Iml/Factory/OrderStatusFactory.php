<?php

namespace Carriers\Iml\Factory;


use Carriers\Iml\Model\OrderStatus;
use Carriers\Iml\Model\OrderStatusDetails;


/**
 * Создаем DTO статуса заказа из полученного ответа API
 *
 * Class OrderStatusFactory
 * @package Carriers\Iml\Factory
 */
class OrderStatusFactory extends AbstractFactory
{
    /**
     * Создадим экземпляр статус-заказа
     *
     * @param array $data
     *
     * @return OrderStatus
     */
    public function create(array $data = [])
    {
        $DetailsItems = [];
        if (!empty($data['Details'])) {
            foreach ($data['Details'] as $detail_item) {
                $DetailsItems[] = $this->hydrate(new OrderStatusDetails(), $detail_item);
            }
            unset($data['Details']);
        }

        /** @var OrderStatus $model */
        $model = $this->hydrate(new OrderStatus(), $data);
        $model->setDetails($DetailsItems);

        return $model;
    }
}
