<?php

namespace Carriers\Iml\Factory;

use Carriers\Iml\Model\Status;

/**
 * Создаем DTO статуса заказа из полученного ответа API
 *
 * Class StatusFactory
 * @package Carriers\Iml\Factory
 */
class StatusFactory extends AbstractFactory
{
    /**
     * @param array $data
     *
     * @return \Carriers\Iml\Model\AbstractModel
     */
    public function create(array $data = [])
    {
        return $this->hydrate(new Status(), $data);
    }
}
