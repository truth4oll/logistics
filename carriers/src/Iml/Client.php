<?php

namespace Carriers\Iml;

use Carriers\Iml\Api\Order;
use Carriers\Iml\Api\Pickup;
use Carriers\Iml\Api\Price;
use Carriers\Iml\Api\Region;
use Carriers\Iml\Api\Status;
use Carriers\Iml\Api\Service;
use Carriers\Iml\Http\HttpClient;

/**
 * Клиент для работы с API IML
 *
 * Class Client
 * @package Carriers\Iml
 */
class Client
{
    /** адрес для запросов работы с заказами (новое api)*/
    const SERVICE_API_URI = 'https://api.iml.ru';

    /** Адрес для запросов к справочникам (старое api)*/
    const SERVICE_LIST_URI = 'https://list.iml.ru';

    /** @var $httpClient HttpClient */
    private $httpClient;

    /** @var array  параметры для HttpClient Р*/
    private $options = [];

    /** @var  \string содержит в себе реквест последнего запроса */
    public static $lastRequest;

    /** @var  \string содержит в себе ответ последнего запроса */
    public static $lastResponse;

    /** @var  \int содержит в себе статус последнего ответа */
    public static $lastStatus;

    /**
     * Client constructor.
     *
     * @param $user
     * @param $password
     * @param $battle
     */
    public function __construct($user, $password, $battle = false)
    {
        if (empty($user) || empty($password)) {
            throw new \InvalidArgumentException(
                "login & password must be not empty"
            );
        }

        //basic auth
        $options['auth'] = [
            $user,
            $password,
        ];

        if (!$battle){
            $options['body']['Test'] = 'True';
        }

        $this->setOptions($options);

        $this->httpClient = new HttpClient($this->getOptions());
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }


    /**
     * @param $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }


    /**
     * @return Pickup
     */
    public function getPickupApi()
    {
        return new Pickup($this);
    }

    /**
     * @return Price
     */
    public function getPriceApi()
    {
        return new Price($this);
    }

    /**
     * Api работы с заказками
     *
     * @return Order
     */
    public function getOrderApi()
    {
        return new Order($this);
    }

    /**
     * Api работы с регионами
     *
     * @return Region
     */
    public function getRegionApi()
    {
        return new Region($this);
    }

    /**
     * @return Status
     */
    public function getStatusesApi()
    {
        return new Status($this);
    }

    /**
     * @return Service
     */
    public function getServiceApi()
    {
        return new Service($this);
    }
}