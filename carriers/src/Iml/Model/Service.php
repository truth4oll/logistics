<?php

namespace Carriers\Iml\Model;

/**
 * Class Service
 * @package Carriers\Iml\Model
 */
class Service extends AbstractModel
{

    /**
     * @var
     */
    private $Code;

    /**
     * @var
     */
    private $Description;

    /**
     * @var
     */
    private $Type;

    /**
     * @var
     */
    private $CommissionType;

    /**
     * @var
     */
    private $ReceiptFromCustomer;

    /**
     * @var
     */
    private $AmountMAX;

    /**
     * @var
     */
    private $AmountMIN;

    /**
     * @var
     */
    private $ValuatedAmountMIN;

    /**
     * @var
     */
    private $ValuatedAmountMAX;

    /**
     * @var
     */
    private $WeightMIN;

    /**
     * @var
     */
    private $WeightMAX;

    /**
     * @var
     */
    private $VolumeMIN;

    /**
     * @var
     */
    private $VolumeMAX;

    /**
     * @var
     */
    private $Scope;

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->Code;
    }

    /**
     * @param mixed $Code
     */
    public function setCode($Code)
    {
        $this->Code = $Code;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param mixed $Description
     */
    public function setDescription($Description)
    {
        $this->Description = $Description;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->Type;
    }

    /**
     * @param mixed $Type
     */
    public function setType($Type)
    {
        $this->Type = $Type;
    }

    /**
     * @return mixed
     */
    public function getCommissionType()
    {
        return $this->CommissionType;
    }

    /**
     * @param mixed $CommissionType
     */
    public function setCommissionType($CommissionType)
    {
        $this->CommissionType = $CommissionType;
    }

    /**
     * @return mixed
     */
    public function getReceiptFromCustomer()
    {
        return $this->ReceiptFromCustomer;
    }

    /**
     * @param mixed $ReceiptFromCustomer
     */
    public function setReceiptFromCustomer($ReceiptFromCustomer)
    {
        $this->ReceiptFromCustomer = $ReceiptFromCustomer;
    }

    /**
     * @return mixed
     */
    public function getAmountMAX()
    {
        return $this->AmountMAX;
    }

    /**
     * @param mixed $AmountMAX
     */
    public function setAmountMAX($AmountMAX)
    {
        $this->AmountMAX = $AmountMAX;
    }

    /**
     * @return mixed
     */
    public function getAmountMIN()
    {
        return $this->AmountMIN;
    }

    /**
     * @param mixed $AmountMIN
     */
    public function setAmountMIN($AmountMIN)
    {
        $this->AmountMIN = $AmountMIN;
    }

    /**
     * @return mixed
     */
    public function getValuatedAmountMIN()
    {
        return $this->ValuatedAmountMIN;
    }

    /**
     * @param mixed $ValuatedAmountMIN
     */
    public function setValuatedAmountMIN($ValuatedAmountMIN)
    {
        $this->ValuatedAmountMIN = $ValuatedAmountMIN;
    }

    /**
     * @return mixed
     */
    public function getValuatedAmountMAX()
    {
        return $this->ValuatedAmountMAX;
    }

    /**
     * @param mixed $ValuatedAmountMAX
     */
    public function setValuatedAmountMAX($ValuatedAmountMAX)
    {
        $this->ValuatedAmountMAX = $ValuatedAmountMAX;
    }

    /**
     * @return mixed
     */
    public function getWeightMIN()
    {
        return $this->WeightMIN;
    }

    /**
     * @param mixed $WeightMIN
     */
    public function setWeightMIN($WeightMIN)
    {
        $this->WeightMIN = $WeightMIN;
    }

    /**
     * @return mixed
     */
    public function getWeightMAX()
    {
        return $this->WeightMAX;
    }

    /**
     * @param mixed $WeightMAX
     */
    public function setWeightMAX($WeightMAX)
    {
        $this->WeightMAX = $WeightMAX;
    }

    /**
     * @return mixed
     */
    public function getVolumeMIN()
    {
        return $this->VolumeMIN;
    }

    /**
     * @param mixed $VolumeMIN
     */
    public function setVolumeMIN($VolumeMIN)
    {
        $this->VolumeMIN = $VolumeMIN;
    }

    /**
     * @return mixed
     */
    public function getVolumeMAX()
    {
        return $this->VolumeMAX;
    }

    /**
     * @param mixed $VolumeMAX
     */
    public function setVolumeMAX($VolumeMAX)
    {
        $this->VolumeMAX = $VolumeMAX;
    }

    /**
     * @return mixed
     */
    public function getScope()
    {
        return $this->Scope;
    }

    /**
     * @param mixed $Scope
     */
    public function setScope($Scope)
    {
        $this->Scope = $Scope;
    }


}