<?php

namespace Carriers\Iml\Model;


/**
 * Ответ IML по запросу получения статусов заказов
 *
 * Class OrderStatus
 * @package Carriers\Iml\Model
 */
class OrderStatus extends AbstractModel
{

    /**
     * @var
     */
    private $Number;
    /**
     * @var
     */
    private $State;
    /**
     * @var
     */
    private $OrderStatus;
    /**
     * @var
     */
    private $StateDescription;
    /**
     * @var
     */
    private $OrderStatusDescription;
    /**
     * @var
     */
    private $StatusDate;
    /**
     * @var
     */
    private $ReturnPayment;
    /**
     * @var
     */
    private $BarCode;
    /**
     * @var
     */
    private $ReturnStatus;
    /**
     * @var
     */
    private $CashReceiptAmount;
    /**
     * @var
     */
    private $Details;

    /**
     * @var
     */
    private $DeliveryDate;

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->Number;
    }

    /**
     * @param mixed $Number
     */
    public function setNumber($Number)
    {
        $this->Number = $Number;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->State;
    }

    /**
     * @param mixed $State
     */
    public function setState($State)
    {
        $this->State = $State;
    }

    /**
     * @return mixed
     */
    public function getOrderStatus()
    {
        return $this->OrderStatus;
    }

    /**
     * @param mixed $OrderStatus
     */
    public function setOrderStatus($OrderStatus)
    {
        $this->OrderStatus = $OrderStatus;
    }

    /**
     * @return mixed
     */
    public function getStateDescription()
    {
        return $this->StateDescription;
    }

    /**
     * @param mixed $StateDescription
     */
    public function setStateDescription($StateDescription)
    {
        $this->StateDescription = $StateDescription;
    }

    /**
     * @return mixed
     */
    public function getOrderStatusDescription()
    {
        return $this->OrderStatusDescription;
    }

    /**
     * @param mixed $OrderStatusDescription
     */
    public function setOrderStatusDescription($OrderStatusDescription)
    {
        $this->OrderStatusDescription = $OrderStatusDescription;
    }

    /**
     * @return mixed
     */
    public function getStatusDate()
    {
        return $this->StatusDate;
    }

    /**
     * @param mixed $StatusDate
     */
    public function setStatusDate($StatusDate)
    {
        $this->StatusDate = $StatusDate;
    }

    /**
     * @return mixed
     */
    public function getReturnPayment()
    {
        return $this->ReturnPayment;
    }

    /**
     * @param mixed $ReturnPayment
     */
    public function setReturnPayment($ReturnPayment)
    {
        $this->ReturnPayment = $ReturnPayment;
    }

    /**
     * @return mixed
     */
    public function getBarCode()
    {
        return $this->BarCode;
    }

    /**
     * @param mixed $BarCode
     */
    public function setBarCode($BarCode)
    {
        $this->BarCode = $BarCode;
    }

    /**
     * @return mixed
     */
    public function getReturnStatus()
    {
        return $this->ReturnStatus;
    }

    /**
     * @param mixed $ReturnStatus
     */
    public function setReturnStatus($ReturnStatus)
    {
        $this->ReturnStatus = $ReturnStatus;
    }

    /**
     * @return mixed
     */
    public function getCashReceiptAmount()
    {
        return $this->CashReceiptAmount;
    }

    /**
     * @param mixed $CashReceiptAmount
     */
    public function setCashReceiptAmount($CashReceiptAmount)
    {
        $this->CashReceiptAmount = $CashReceiptAmount;
    }

    /**
     * Позиции заказа
     *
     * @return OrderStatusDetails[]
     */
    public function getDetails()
    {
        return $this->Details;
    }

    /**
     * @param mixed $Details
     */
    public function setDetails($Details)
    {
        $this->Details = $Details;
    }

    /**
     * @return mixed
     */
    public function getDeliveryDate()
    {
        return $this->DeliveryDate;
    }

    /**
     * @param mixed $DeliveryDate
     */
    public function setDeliveryDate($DeliveryDate)
    {
        $this->DeliveryDate = $DeliveryDate;
    }

    /**
     * Получить объект статуса в виде массива
     * @return array
     */
    public function toArray() {
        $vars = get_object_vars ( $this );
        $array = array ();
        foreach ( $vars as $key => $value ) {
            $array [ltrim ( $key, '_' )] = $value;
        }
        return $array;
    }
}