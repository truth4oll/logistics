<?php

namespace Carriers\Iml\Model;


/**
 * Модель заказа
 *
 * Class Order
 * @package Carriers\Iml\Model
 */
class Order extends AbstractModel
{
    protected $job;

    protected $customerOrder;

    protected $deliveryDate;

    protected $volume;

    protected $weight;

    protected $barCode;

    protected $deliveryPoint;

    protected $phone;

    protected $email;

    protected $contact;

    protected $indexFrom;

    protected $indexTo;

    protected $regionCodeFrom;

    protected $regionCodeTo;

    protected $address;

    protected $timeFrom;

    protected $timeTo;

    protected $amount;

    protected $valuatedAmount;

    protected $comment;

    protected $city;

    protected $postCode;

    protected $postRegion;

    protected $postArea;

    protected $postContentType;

    protected $draft;

    protected $goodItems;

    /**
     * услуга доставки
     *
     * @return mixed
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param mixed $job
     */
    public function setJob($job)
    {
        $this->job = $job;
    }

    /**
     * номер заказа
     *
     * @return mixed
     */
    public function getCustomerOrder()
    {
        return $this->customerOrder;
    }

    /**
     * @param mixed $customerOrder
     */
    public function setCustomerOrder($customerOrder)
    {
        $this->customerOrder = $customerOrder;
    }

    /**
     *  дата доставки в строковом представлении
     *
     * @return mixed
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     *
     * @param mixed $deliveryDate
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * количество мест
     *
     * @return mixed
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @param mixed $volume
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;
    }

    /**
     * вес
     *
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * штрих код заказа в формате EAN-13
     *
     * @return mixed
     */
    public function getBarCode()
    {
        return $this->barCode;
    }

    /**
     * @param mixed $barCode
     */
    public function setBarCode($barCode)
    {
        $this->barCode = $barCode;
    }

    /**
     * пункта самовывоза, Code из таблицы пунктов самовывоза
     *
     * @return mixed
     */
    public function getDeliveryPoint()
    {
        return $this->deliveryPoint;
    }

    /**
     * @param mixed $deliveryPoint
     */
    public function setDeliveryPoint($deliveryPoint)
    {
        $this->deliveryPoint = $deliveryPoint;
    }

    /**
     * телефон
     *
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * контактное лицо
     *
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * код региона отправления
     *
     * @return mixed
     */
    public function getIndexFrom()
    {
        return $this->indexFrom;
    }

    /**
     * @param mixed $indexFrom
     */
    public function setIndexFrom($indexFrom)
    {
        $this->indexFrom = $indexFrom;
    }

    /**
     * код региона получения
     *
     * @return mixed
     */
    public function getIndexTo()
    {
        return $this->indexTo;
    }

    /**
     * @param mixed $indexTo
     */
    public function setIndexTo($indexTo)
    {
        $this->indexTo = $indexTo;
    }

    /**
     * @return mixed
     */
    public function getRegionCodeFrom()
    {
        return $this->regionCodeFrom;
    }

    /**
     * @param mixed $regionCodeFrom
     */
    public function setRegionCodeFrom($regionCodeFrom)
    {
        $this->regionCodeFrom = $regionCodeFrom;
    }

    /**
     * @return mixed
     */
    public function getRegionCodeTo()
    {
        return $this->regionCodeTo;
    }

    /**
     * @param mixed $regionCodeTo
     */
    public function setRegionCodeTo($regionCodeTo)
    {
        $this->regionCodeTo = $regionCodeTo;
    }

    /**
     * адрес доставки
     *
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     *  начало временного периода доставки
     *
     * @return mixed
     */
    public function getTimeFrom()
    {
        return $this->timeFrom;
    }

    /**
     * @param mixed $timeFrom
     */
    public function setTimeFrom($timeFrom)
    {
        $this->timeFrom = $timeFrom;
    }

    /**
     * конец временного периода доставки
     *
     * @return mixed
     */
    public function getTimeTo()
    {
        return $this->timeTo;
    }

    /**
     * @param mixed $timeTo
     */
    public function setTimeTo($timeTo)
    {
        $this->timeTo = $timeTo;
    }

    /**
     *  сумма заказа
     *
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * оценочная стоимость заказа
     *
     * @return mixed
     */
    public function getValuatedAmount()
    {
        return $this->valuatedAmount;
    }

    /**
     * @param mixed $valuatedAmount
     */
    public function setValuatedAmount($valuatedAmount)
    {
        $this->valuatedAmount = $valuatedAmount;
    }

    /**
     * комментарий
     *
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * @param mixed $postCode
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;
    }

    /**
     * @return mixed
     */
    public function getPostRegion()
    {
        return $this->postRegion;
    }

    /**
     * @param mixed $postRegion
     */
    public function setPostRegion($postRegion)
    {
        $this->postRegion = $postRegion;
    }

    /**
     * @return mixed
     */
    public function getPostArea()
    {
        return $this->postArea;
    }

    /**
     * @param mixed $postArea
     */
    public function setPostArea($postArea)
    {
        $this->postArea = $postArea;
    }

    /**
     * @return mixed
     */
    public function getPostContentType()
    {
        return $this->postContentType;
    }

    /**
     * @param mixed $postContentType
     */
    public function setPostContentType($postContentType)
    {
        $this->postContentType = $postContentType;
    }

    /**
     * @return mixed
     */
    public function getDraft()
    {
        return $this->draft;
    }

    /**
     * @param mixed $draft
     */
    public function setDraft($draft)
    {
        $this->draft = $draft;
    }

    /**
     * позиции заказа, если указывались при создании заказа, тип значения – массив элементов с параметрами:
     *
     * @return mixed
     */
    public function getGoodItems()
    {
        return $this->goodItems;
    }

    /**
     * @param OrderItem[] $goodItems
     */
    public function setOrderItems($goodItems)
    {
        $this->goodItems = $goodItems;
    }




}