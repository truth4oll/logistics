<?php

namespace Carriers\Iml\Model;

/**
 * DTO региона, полученного из API
 * @see Region
 *
 * Class Region
 * @package Carriers\Iml\Model
 */
class Region extends AbstractModel
{

    /**
     * @var
     */
    private $Code;

    /**
     * @var
     */
    private $Description;

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->Code;
    }

    /**
     * @param mixed $Code
     */
    public function setCode($Code)
    {
        $this->Code = $Code;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param mixed $Description
     */
    public function setDescription($Description)
    {
        $this->Description = $Description;
    }




}