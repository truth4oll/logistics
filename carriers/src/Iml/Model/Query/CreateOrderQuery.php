<?php

namespace Carriers\Iml\Model\Query;


/**
 *
 * Класс для подготовки запроса создания заказа
 *
 * Class CreateOrderQuery
 * @package Carriers\Iml\Model\Query
 */
class CreateOrderQuery extends AbstractQuery
{

    /**
     * тестовый режим, 'True' для тестового режима, иначе не указывайте
     *
     * @param mixed $Test
     *
     * @return $this
     */
    public function setTest($Test)
    {
        $this->set('Test', $Test);

        return $this;
    }

    /**
     * услуга доставки, Code из справочника услуг
     *
     * @param mixed $Job
     *
     * @return $this
     */
    public function setJob($Job)
    {
        $this->set('Job', $Job);

        return $this;
    }

    /**
     * номер заказа
     *
     * @param mixed $CustomerOrder
     *
     * @return $this
     */
    public function setCustomerOrder($CustomerOrder)
    {
        $this->set('CustomerOrder', $CustomerOrder);

        return $this;
    }

    /**
     * дата доставки в строковом представлении, формат dd.MM.yyyy
     *
     * @param mixed $DeliveryDate
     *
     * @return $this
     */
    public function setDeliveryDate($DeliveryDate)
    {
        $this->set('DeliveryDate', $DeliveryDate);

        return $this;
    }

    /**
     *  количество мест
     *
     * @param mixed $Volume
     *
     * @return $this
     */
    public function setVolume($Volume)
    {
        $this->set('Volume', $Volume);

        return $this;
    }

    /**
     * вес
     *
     * @param mixed $Weight
     *
     * @return $this
     */
    public function setWeight($Weight)
    {
        $this->set('Weight', $Weight);

        return $this;
    }

    /**
     * штрих код заказа в формате EAN-13
     *
     * @param mixed $BarCode
     *
     * @return $this
     */
    public function setBarCode($BarCode)
    {
        $this->set('BarCode', $BarCode);

        return $this;
    }

    /**
     * пункта самовывоза, RequestCode из таблицы пунктов самовывоза
     *
     * @param mixed $DeliveryPoint
     *
     * @return $this
     */
    public function setDeliveryPoint($DeliveryPoint)
    {
        $this->set('DeliveryPoint', $DeliveryPoint);

        return $this;
    }

    /**
     * телефон
     *
     * @param mixed $Phone
     *
     * @return $this
     */
    public function setPhone($Phone)
    {
        $this->set('Phone', $Phone);

        return $this;
    }

    /**
     * электронный адрес получателя заказа
     *
     * @param mixed $Email
     *
     * @return $this
     */
    public function setEmail($Email)
    {
        $this->set('Email', $Email);

        return $this;
    }

    /**
     * контактное лицо
     *
     * @param mixed $Contact
     *
     * @return $this
     */
    public function setContact($Contact)
    {
        $this->set('Contact', $Contact);

        return $this;
    }

    /**
     *  индекс региона отправления, альтернатива RegionCodeFrom
     *
     * @param mixed $IndexFrom
     *
     * @return $this
     */
    public function setIndexFrom($IndexFrom)
    {
        $this->set('IndexFrom', $IndexFrom);

        return $this;
    }

    /**
     *  индекс региона получения, альтернатива RegionCodeTo
     *
     * @param mixed $IndexTo
     *
     * @return $this
     */
    public function setIndexTo($IndexTo)
    {
        $this->set('IndexTo', $IndexTo);

        return $this;
    }

    /**
     *  код региона отправления, Code из таблицы регионов
     *
     * @param mixed $RegionCodeFrom
     *
     * @return $this
     */
    public function setRegionCodeFrom($RegionCodeFrom)
    {
        $this->set('RegionCodeFrom', $RegionCodeFrom);

        return $this;
    }

    /**
     * код региона получения, Code из таблицы регионов
     *
     * @param mixed $RegionCodeTo
     *
     * @return $this
     */
    public function setRegionCodeTo($RegionCodeTo)
    {
        $this->set('RegionCodeTo', $RegionCodeTo);

        return $this;
    }

    /**
     *  адрес доставки
     *
     * @param mixed $Address
     *
     * @return $this
     */
    public function setAddress($Address)
    {
        $this->set('Address', $Address);

        return $this;
    }

    /**
     *  начало временного периода доставки
     *
     * @param mixed $TimeFrom
     *
     * @return $this
     */
    public function setTimeFrom($TimeFrom)
    {
        $this->set('TimeFrom', $TimeFrom);

        return $this;
    }

    /**
     *  конец временного периода доставки
     *
     * @param mixed $TimeTo
     *
     * @return $this
     */
    public function setTimeTo($TimeTo)
    {
        $this->set('TimeTo', $TimeTo);

        return $this;
    }

    /**
     * сумма заказа
     *
     * @param mixed $Amount
     *
     * @return $this
     */
    public function setAmount($Amount)
    {
        $this->set('Amount', $Amount);

        return $this;
    }

    /**
     *  оценочная стоимость заказа
     *
     * @param mixed $ValuatedAmount
     *
     * @return $this
     */
    public function setValuatedAmount($ValuatedAmount)
    {
        $this->set('ValuatedAmount', $ValuatedAmount);

        return $this;
    }

    /**
     * комментарий
     *
     * @param mixed $Comment
     *
     * @return $this
     */
    public function setComment($Comment)
    {
        $this->set('Comment', $Comment);

        return $this;
    }

    /**
     * город доставки, для отправки почтой России
     *
     * @param mixed $City
     *
     * @return $this
     */
    public function setCity($City)
    {
        $this->set('City', $City);

        return $this;
    }

    /**
     * индекс, для отправки почтой России
     *
     * @param mixed $PostCode
     *
     * @return $this
     */
    public function setPostCode($PostCode)
    {
        $this->set('PostCode', $PostCode);

        return $this;
    }

    /**
     *  регион, для отправки почтой России
     *
     * @param mixed $PostRegion
     *
     * @return $this
     */
    public function setPostRegion($PostRegion)
    {
        $this->set('PostRegion', $PostRegion);

        return $this;
    }

    /**
     *  район, для отправки почтой России
     *
     * @param mixed $PostArea
     *
     * @return $this
     */
    public function setPostArea($PostArea)
    {
        $this->set('PostArea', $PostArea);

        return $this;
    }

    /**
     * тип вложения (0 - Печатная, 1 - Разное, 2 - 1 Класс), для отправки почтой России
     *
     * @param mixed $PostContentType
     *
     * @return $this
     */
    public function setPostContentType($PostContentType)
    {
        $this->set('PostContentType', $PostContentType);

        return $this;
    }

    /**
     * фраг обработки заказа. 0 - означает что заказ грузится в список "просмотр" ЛК,
     * 1 - означает что заказ грузится в список "импорт" ЛК. Если параметр отсутствует, то считается, что draft = 0.
     *
     * @param mixed $Draft
     *
     * @return $this
     */
    public function setDraft($Draft)
    {
        $this->set('Draft', $Draft);

        return $this;
    }

    /**
     *
     * Позиции заказа
     *
     * @see CreateOrderQuery
     *
     * @param CreateOrderQuery[] $goodItems
     *
     * @return $this
     */
    public function setGoodItems($goodItems)
    {
        $goodItemsArray = [];
        foreach ($goodItems as $goodItem) {
            $goodItemsArray[] = $goodItem->toArray();
        }
        $this->set('goodItems', $goodItemsArray);

        return $this;
    }


}
