<?php

namespace Carriers\Iml\Model\Query;

/**
 * Класс для подготовки запроса получения заказов
 *
 * Class GetOrdersQuery
 * @package Carriers\Iml\Model\Query
 */
class GetOrdersQuery extends AbstractQuery
{

    /**
     * Тестовый режим, 'True' для тестового режима, иначе не указывайте
     *
     * @param mixed $Test
     *
     * @return $this
     */
    public function setTest($Test)
    {
        $this->set('Test', $Test);

        return $this;
    }

    /**
     * Услуга, Code из справочника услуг
     *
     * @param mixed $Job
     *
     * @return $this
     */
    public function setJob($Job)
    {
        $this->set('Job', $Job);

        return $this;
    }

    /**
     * номер заказа
     *
     * @param mixed $CustomerOrder
     *
     * @return $this
     */
    public function setCustomerOrder($CustomerOrder)
    {
        $this->set('CustomerOrder', $CustomerOrder);

        return $this;
    }

    /**
     *  дата доставки, запросы с датой доставки начиная с указанной (включительно), строка в формате ‘dd.MM.yyyy’
     *
     * @param mixed $DeliveryDate
     *
     * @return $this
     */
    public function setDeliveryDateStart($DeliveryDate)
    {
        $this->set('DeliveryDateStart', $DeliveryDate);

        return $this;
    }


    /**
     * дата доставки, запросы с датой доставки по указанную, строка в формате ‘dd.MM.yyyy’
     *
     * @param mixed $DeliveryDate
     *
     * @return $this
     */
    public function setDeliveryDateEnd($DeliveryDate)
    {
        $this->set('DeliveryDateStart', $DeliveryDate);

        return $this;
    }

    /**
     * состояние заказа, Code из таблицы состояний заказа
     *
     * @param mixed $state
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->set('State', $state);

        return $this;
    }


    /**
     * статус заказа, Code из таблицы статусов заказа
     *
     * @param mixed $status
     *
     * @return $this
     */
    public function setOrderStatus($status)
    {
        $this->set('OrderStatus', $status);

        return $this;
    }


    /**
     * дата создания, начиная с указанной (включительно), тип значения строка в формате ‘dd.MM.yyyy’
     *
     * @param $date
     *
     * @return $this
     */
    public function setCreationDateStart($date)
    {
        $this->set('CreationDateStart', $date);

        return $this;
    }

    /**
     * конечная дата создания, тип значения строка в формате ‘dd.MM.yyyy’
     *
     * @param $date
     *
     * @return $this
     */
    public function setCreationDateEnd($date)
    {
        $this->set('CreationDateEnd', $date);

        return $this;
    }


    /**
     *  регион получения, Code из таблицы регионов
     *
     * @param $region
     *
     * @return $this
     */
    public function setRegionTo($region)
    {
        $this->set('RegionTo', $region);

        return $this;
    }


    /**
     * регион отправления, Code из таблицы регионов
     *
     * @param $region
     *
     * @return $this
     */
    public function setRegionFrom($region)
    {
        $this->set('RegionFrom', $region);

        return $this;
    }

    /**
     * штрих код заказа в формате EAN-13
     *
     * @param $barcode
     *
     * @return $this
     */
    public function setBarCode($barcode)
    {
        $this->set('BarCode', $barcode);

        return $this;
    }


}
