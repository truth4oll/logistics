<?php

namespace Carriers\Iml\Model\Query\Type;

use Carriers\Iml\Model\Query\CreateOrderQuery;

/**
 * Вес заказа, необходим при создании заказа
 * @see CreateOrderQuery
 *
 * Class Weight
 * @package Carriers\Iml\Model\Query\Type
 */
class Weight
{

    private $weight;

    public function __construct($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Стандартизируем отправляймый вес
     * @return string
     */
    public function __toString()
    {
        $this->weight = str_replace(',', '.', $this->weight);
        $this->weight = ceil($this->weight);

        return (string)$this->weight;
    }
}