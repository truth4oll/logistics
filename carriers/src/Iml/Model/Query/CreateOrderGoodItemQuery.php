<?php

namespace Carriers\Iml\Model\Query;


/**
 * Подготовка массива для отправки запроса позиций заказа
 * @see CreateOrderQuery
 *
 * @see \Carriers\Iml\Api\Order
 *
 * Class CreateOrderGoodItemQuery
 * @package Carriers\Iml\Model\Query
 */
class CreateOrderGoodItemQuery extends AbstractQuery
{

    /**
     * Товар
     */
    const STATUS_PRODUCT = 0;

    /**
     * Сопутствующий товар
     */
    const STATUS_PRODUCT_JOINT = 1;

    /**
     * Рекламный материал
     */
    const STATUS_PRODUCT_ADV = 2;

    /**
     * Услуга доставки
     */
    const STATUS_SERVICE = 3;

    /**
     * Удаление
     */
    const STATUS_REMOVE = 5;

    /**
     * Грузовое место
     */
    const STATUS_PACKAGE = 7;


    /**
     * номер товара (артикул)
     *
     * @param mixed $productNo
     *
     * @return $this
     */
    public function setProductNo($productNo)
    {
        $this->set('productNo', $productNo);

        return $this;
    }

    /**
     * наименование товара
     *
     * @param mixed $productName
     *
     * @return $this
     */
    public function setProductName($productName)
    {
        $this->set('productName', $productName);

        return $this;
    }

    /**
     * вариант товара (размер, цвет и т.д)
     *
     * @param mixed $productVariant
     *
     * @return $this
     */
    public function setProductVariant($productVariant)
    {
        $this->set('productVariant', $productVariant);

        return $this;
    }

    /**
     * штрих код продукта
     *
     * @param mixed $productBarCode
     *
     * @return $this
     */
    public function setProductBarCode($productBarCode)
    {
        $this->set('productBarCode', $productBarCode);

        return $this;
    }

    /**
     * номер купона
     *
     * @param mixed $couponCode
     *
     * @return $this
     */
    public function setCouponCode($couponCode)
    {
        $this->set('couponCode', $couponCode);

        return $this;
    }

    /**
     * скидка
     *
     * @param mixed $discount
     *
     * @return $this
     */
    public function setDiscount($discount)
    {
        $this->set('discount', $discount);

        return $this;
    }

    /**
     * вес товара
     *
     * @param mixed $weightLine
     *
     * @return $this
     */
    public function setWeightLine($weightLine)
    {
        $this->set('weightLine', $weightLine);

        return $this;
    }

    /**
     * стоимость товара
     *
     * @param mixed $amountLine
     *
     * @return $this
     */
    public function setAmountLine($amountLine)
    {
        $this->set('amountLine', $amountLine);

        return $this;
    }

    /**
     * оценочная стоимость товара
     *
     * @param mixed $statisticalValueLine
     *
     * @return $this
     */
    public function setStatisticalValueLine($statisticalValueLine)
    {
        $this->set('statisticalValueLine', $statisticalValueLine);

        return $this;
    }

    /**
     * количество товара
     *
     * @param mixed $itemQuantity
     *
     * @return $this
     */
    public function setItemQuantity($itemQuantity)
    {
        $this->set('itemQuantity', $itemQuantity);

        return $this;
    }

    /**
     * запрет отказа от позиции при частичной выдаче заказа [1,0]
     *
     * @param mixed $deliveryService
     *
     * @return $this
     */
    public function setDeliveryService($deliveryService)
    {
        $this->set('deliveryService', $deliveryService);

        return $this;
    }

    /**
     * тип подробности заказа
     *
     * @param mixed $itemType
     *
     * @return $this
     */
    public function setItemType($itemType)
    {
        $this->set('itemType', $itemType);

        return $this;
    }

    /**
     * примечание к подробности заказа, комментарий который вы хотели бы донести до сотрудника IML
     *
     * @param mixed $itemNote
     *
     * @return $this
     */
    public function setItemNote($itemNote)
    {
        $this->set('itemNote', $itemNote);

        return $this;
    }

    /**
     * используется для разрешения или запрета дополнительных условий выдачи заказа (запретить или разрешить
     * частичную выдачу заказа, запретить примерку, запретить вскрытие упаковки заказа и пр.).
     * Используется для itemType = 10
     *
     * @param mixed $allowed
     *
     * @return $this
     */
    public function setAllowed($allowed)
    {
        $this->set('allowed', $allowed);

        return $this;
    }

    /**
     * размер НДС в процентах.
     *
     * @param mixed $VATRate
     *
     * @return $this
     */
    public function setVATRate($VATRate)
    {
        $this->set('VATRate', $VATRate);

        return $this;
    }

    /**
     * размер НДС в рублях.
     *
     * @param mixed $VATAmount
     *
     * @return $this
     */
    public function setVATAmount($VATAmount)
    {
        $this->set('VATAmount', $VATAmount);

        return $this;
    }


}
