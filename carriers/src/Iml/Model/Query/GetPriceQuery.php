<?php

namespace Carriers\Iml\Model\Query;


use Carriers\Iml\Model\Query\Type\Weight;

/**
 * Класс для формирования запроса к api получаения цены
 * @see \Carriers\Iml\Api\Price
 *
 * Class GetPriceQuery
 * @package Carriers\Iml\Model\Query
 */
class GetPriceQuery extends AbstractQuery
{

    /**
     * @param $job
     *
     * @return $this
     */
    public function job($job)
    {
        $this->set('Job', $job);

        return $this;
    }


    /**
     * @param $regionFrom
     *
     * @return $this
     */
    public function regionFrom($regionFrom)
    {
        $this->set('RegionFrom', $regionFrom);

        return $this;
    }

    /**
     * @param $regionTo
     *
     * @return $this
     */
    public function regionTo($regionTo)
    {
        $this->set('RegionTo', $regionTo);

        return $this;
    }


    /**
     * @param $volume
     *
     * @return $this
     */
    public function volume($volume)
    {
        $this->set('Volume', $volume);

        return $this;
    }


    /**
     * Вес в киллограммах
     *
     * @param Weight $weight
     *
     * @return $this
     */
    public function weight(Weight $weight)
    {
        $this->set('Weigth', $weight);

        return $this;
    }

    public function toArray()
    {
        $weight = $this->get('Weigth');
        $this->set('Weigth', (string)$weight);
        return parent::toArray();
    }


}
