<?php

namespace Carriers\Iml\Model\Query;

abstract class AbstractQuery implements \ArrayAccess, \IteratorAggregate, \Countable
{

    protected $data = [];


    public function __construct(array $data = [])
    {
        $this->data = $data;
    }


    /**
     * @return int
     */
    public function count()
    {
        return count($this->data);
    }

    public function toArray()
    {
        return $this->data;
    }


    public function set($key, $value)
    {
        $this->data[$key] = $value;

        return $this;
    }

    public function get($key)
    {
        return $this->data[$key];
    }

    /**
     * @return \ArrayIterator|\Traversable
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->data);
    }

    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }

    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }


}