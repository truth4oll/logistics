<?php

namespace Carriers\Iml\Model\Collection;

use Carriers\Iml\Model\AbstractModel;


/**
 *
 * Коллекция статусов
 *
 * Class Statuses
 * @package Carriers\Iml\Model\Collection
 */
class Statuses extends AbstractCollection
{
    /**
     * @return array
     */
    public function getStatuses()
    {
        return $this->data;
    }


    /**
     * @param AbstractModel $model
     *
     * @return $this
     */
    public function addStatus(AbstractModel $model)
    {
        $this->data[] = $model;

        return $this;
    }

}