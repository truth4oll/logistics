<?php

namespace Carriers\Iml\Model\Collection;

use Carriers\Iml\Model\AbstractModel;


/**
 *
 * Коллекция регионов
 *
 * Class Statuses
 * @package Carriers\Iml\Model\Collection
 */
class Regions extends AbstractCollection
{
    public function getRegions()
    {
        return $this->data;
    }


    /**
     * Добавим регион в коллекцию
     * @param AbstractModel $model
     *
     * @return $this
     */
    public function addRegion(AbstractModel $model)
    {
        $this->data[] = $model;

        return $this;
    }

}