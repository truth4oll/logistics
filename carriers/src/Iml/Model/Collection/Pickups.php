<?php

namespace Carriers\Iml\Model\Collection;

use Carriers\Iml\Model\AbstractModel;


/**
 *
 * Коллекция ПВЗ
 *
 * Class Pickups
 * @package Carriers\Iml\Model\Collection
 */
class Pickups extends AbstractCollection
{
    /**
     * @return array
     */
    public function getPickups()
    {
        return $this->data;
    }


    /**
     * @param AbstractModel $model
     *
     * @return $this
     */
    public function addPickup(AbstractModel $model)
    {
        $this->data[] = $model;

        return $this;
    }

}