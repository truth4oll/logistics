<?php

namespace Carriers\Iml\Model;

/**
 * Позиция заказа вовзращаемые IML
 *
 * Class OrderItem
 * @package Carriers\Iml\Model
 */
class OrderItem extends AbstractModel
{
    protected $productNo;

    protected $productName;

    protected $productVariant;

    protected $productBarCode;

    protected $couponCode;

    protected $discount;

    protected $weightLine;

    protected $amountLine;

    protected $statisticalValueLine;

    protected $itemQuantity;

    protected $deliveryService;

    protected $itemType;

    protected $itemNote;

    protected $allowed;

    protected $VATRate;

    protected $VATAmount;

    /**
     * идентификатор товара
     * @return mixed
     */
    public function getProductNo()
    {
        return $this->productNo;
    }

    /**
     * @param mixed $productNo
     */
    public function setProductNo($productNo)
    {
        $this->productNo = $productNo;
    }

    /**
     * @return mixed
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param mixed $productName
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;
    }

    /**
     * @return mixed
     */
    public function getProductVariant()
    {
        return $this->productVariant;
    }

    /**
     * @param mixed $productVariant
     */
    public function setProductVariant($productVariant)
    {
        $this->productVariant = $productVariant;
    }

    /**
     * @return mixed
     */
    public function getProductBarCode()
    {
        return $this->productBarCode;
    }

    /**
     * @param mixed $productBarCode
     */
    public function setProductBarCode($productBarCode)
    {
        $this->productBarCode = $productBarCode;
    }

    /**
     * @return mixed
     */
    public function getCouponCode()
    {
        return $this->couponCode;
    }

    /**
     * @param mixed $couponCode
     */
    public function setCouponCode($couponCode)
    {
        $this->couponCode = $couponCode;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getWeightLine()
    {
        return $this->weightLine;
    }

    /**
     * @param mixed $weightLine
     */
    public function setWeightLine($weightLine)
    {
        $this->weightLine = $weightLine;
    }

    /**
     * @return mixed
     */
    public function getAmountLine()
    {
        return $this->amountLine;
    }

    /**
     * @param mixed $amountLine
     */
    public function setAmountLine($amountLine)
    {
        $this->amountLine = $amountLine;
    }

    /**
     * @return mixed
     */
    public function getStatisticalValueLine()
    {
        return $this->statisticalValueLine;
    }

    /**
     * @param mixed $statisticalValueLine
     */
    public function setStatisticalValueLine($statisticalValueLine)
    {
        $this->statisticalValueLine = $statisticalValueLine;
    }

    /**
     * @return mixed
     */
    public function getItemQuantity()
    {
        return $this->itemQuantity;
    }

    /**
     * @param mixed $itemQuantity
     */
    public function setItemQuantity($itemQuantity)
    {
        $this->itemQuantity = $itemQuantity;
    }

    /**
     * @return mixed
     */
    public function getDeliveryService()
    {
        return $this->deliveryService;
    }

    /**
     * @param mixed $deliveryService
     */
    public function setDeliveryService($deliveryService)
    {
        $this->deliveryService = $deliveryService;
    }

    /**
     * @return mixed
     */
    public function getItemType()
    {
        return $this->itemType;
    }

    /**
     * @param mixed $itemType
     */
    public function setItemType($itemType)
    {
        $this->itemType = $itemType;
    }

    /**
     * @return mixed
     */
    public function getItemNote()
    {
        return $this->itemNote;
    }

    /**
     * @param mixed $itemNote
     */
    public function setItemNote($itemNote)
    {
        $this->itemNote = $itemNote;
    }

    /**
     * @return mixed
     */
    public function getAllowed()
    {
        return $this->allowed;
    }

    /**
     * @param mixed $allowed
     */
    public function setAllowed($allowed)
    {
        $this->allowed = $allowed;
    }

    /**
     * @return mixed
     */
    public function getVATRate()
    {
        return $this->VATRate;
    }

    /**
     * @param mixed $VATRate
     */
    public function setVATRate($VATRate)
    {
        $this->VATRate = $VATRate;
    }

    /**
     * @return mixed
     */
    public function getVATAmount()
    {
        return $this->VATAmount;
    }

    /**
     * @param mixed $VATAmount
     */
    public function setVATAmount($VATAmount)
    {
        $this->VATAmount = $VATAmount;
    }


}