<?php

namespace Carriers\Iml\Model;

/**
 * Объект пункта самовывоза
 *
 * Class Pickup
 * @package Carriers\Iml\Model
 */
class Pickup extends AbstractModel
{
    private $address;

    private $closingDate;

    private $code;

    private $couponReceipt;

    private $email;

    private $fittingRoom;

    private $homePage;

    private $index;

    private $latitude;

    private $longtitude;

    private $name;

    private $openingDate;

    private $paymentCard;

    private $phone;

    private $receiptOrder;

    private $regionCode;

    private $requestCode;

    private $workMode;

    /**
     * Получить адрес ПВЗ
     *
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Дата Закрытия
     *
     * @return mixed
     */
    public function getClosingDate()
    {
        return $this->closingDate;
    }

    /**
     * @param mixed $closingDate
     */
    public function setClosingDate($closingDate)
    {
        $this->closingDate = $closingDate;
    }

    /**
     * Код
     *
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * Прием купонов
     *
     * @return mixed
     */
    public function getCouponReceipt()
    {
        return $this->couponReceipt;
    }

    /**
     * @param mixed $couponReceipt
     */
    public function setCouponReceipt($couponReceipt)
    {
        $this->couponReceipt = $couponReceipt;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Примерочная, Кол-во
     *
     * @return mixed
     */
    public function getFittingRoom()
    {
        return $this->fittingRoom;
    }

    /**
     * @param mixed $fittingRoom
     */
    public function setFittingRoom($fittingRoom)
    {
        $this->fittingRoom = $fittingRoom;
    }

    /**
     * Ссылка сайт
     *
     * @return mixed
     */
    public function getHomePage()
    {
        return $this->homePage;
    }

    /**
     * @param mixed $homePage
     */
    public function setHomePage($homePage)
    {
        $this->homePage = $homePage;
    }

    /**
     * Индекс
     *
     * @return mixed
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param mixed $index
     */
    public function setIndex($index)
    {
        $this->index = $index;
    }

    /**
     * Широта
     *
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * Долгота
     *
     * @return mixed
     */
    public function getLongtitude()
    {
        return $this->longtitude;
    }

    /**
     * @param mixed $longtitude
     */
    public function setLongtitude($longtitude)
    {
        $this->longtitude = $longtitude;
    }

    /**
     * Название
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getOpeningDate()
    {
        return $this->openingDate;
    }

    /**
     * Дата Открытия
     *
     * @param mixed $openingDate
     */
    public function setOpeningDate($openingDate)
    {
        $this->openingDate = $openingDate;
    }

    /**
     * Прием ДС по платежным картам, 0 - нет, 1 - да
     *
     * @return mixed
     */
    public function getPaymentCard()
    {
        return $this->paymentCard;
    }

    /**
     * @param mixed $paymentCard
     */
    public function setPaymentCard($paymentCard)
    {
        $this->paymentCard = $paymentCard;
    }

    /**
     * Телефон
     *
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Прием отправлений
     *
     * @return mixed
     */
    public function getReceiptOrder()
    {
        return $this->receiptOrder;
    }

    /**
     * @param mixed $receiptOrder
     */
    public function setReceiptOrder($receiptOrder)
    {
        $this->receiptOrder = $receiptOrder;
    }

    /**
     * Регион
     *
     * @return mixed
     */
    public function getRegionCode()
    {
        return $this->regionCode;
    }

    /**
     * @param mixed $regionCode
     */
    public function setRegionCode($regionCode)
    {
        $this->regionCode = $regionCode;
    }

    /**
     * Код заявки
     *
     * @return mixed
     */
    public function getRequestCode()
    {
        return $this->requestCode;
    }

    /**
     * @param mixed $requestCode
     */
    public function setRequestCode($requestCode)
    {
        $this->requestCode = $requestCode;
    }

    /**
     * Режим работы
     *
     * @return mixed
     */
    public function getWorkMode()
    {
        return $this->workMode;
    }

    /**
     * @param mixed $workMode
     */
    public function setWorkMode($workMode)
    {
        $this->workMode = $workMode;
    }


}