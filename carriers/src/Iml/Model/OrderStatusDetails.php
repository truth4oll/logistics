<?php

namespace Carriers\Iml\Model;


/**
 *
 *
 *
 * Class OrderStatusDetails
 * @package Carriers\Iml\Model
 */
class OrderStatusDetails extends AbstractModel implements \JsonSerializable
{
    /**
     * @var
     */
    private $change;
    /**
     * @var
     */
    private $productNo;
    /**
     * @var
     */
    private $productName;
    /**
     * @var
     */
    private $service;
    /**
     * @var
     */
    private $amount;
    /**
     * @var
     */
    private $cashRecieptAmount;
    /**
     * @var
     */
    private $cancel;
    /**
     * @var
     */
    private $returnCode;
    /**
     * @var
     */
    private $productVariant;
    /**
     * @var
     */
    private $statisticalValueLine;

    /**
     * @return mixed
     */
    public function getChange()
    {
        return $this->change;
    }

    /**
     * @param mixed $change
     */
    public function setChange($change)
    {
        $this->change = $change;
    }

    /**
     * @return mixed
     */
    public function getProductNo()
    {
        return $this->productNo;
    }

    /**
     * @param mixed $productNo
     */
    public function setProductNo($productNo)
    {
        $this->productNo = $productNo;
    }

    /**
     * @return mixed
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param mixed $productName
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getCashRecieptAmount()
    {
        return $this->cashRecieptAmount;
    }

    /**
     * @param mixed $cashRecieptAmount
     */
    public function setCashRecieptAmount($cashRecieptAmount)
    {
        $this->cashRecieptAmount = $cashRecieptAmount;
    }

    /**
     * @return mixed
     */
    public function getCancel()
    {
        return $this->cancel;
    }

    /**
     * @param mixed $cancel
     */
    public function setCancel($cancel)
    {
        $this->cancel = $cancel;
    }

    /**
     * @return mixed
     */
    public function getReturnCode()
    {
        return $this->returnCode;
    }

    /**
     * @param mixed $returnCode
     */
    public function setReturnCode($returnCode)
    {
        $this->returnCode = $returnCode;
    }

    /**
     * @return mixed
     */
    public function getProductVariant()
    {
        return $this->productVariant;
    }

    /**
     * @param mixed $productVariant
     */
    public function setProductVariant($productVariant)
    {
        $this->productVariant = $productVariant;
    }

    /**
     * @return mixed
     */
    public function getStatisticalValueLine()
    {
        return $this->statisticalValueLine;
    }

    /**
     * @param mixed $statisticalValueLine
     */
    public function setStatisticalValueLine($statisticalValueLine)
    {
        $this->statisticalValueLine = $statisticalValueLine;
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return [
            'product_no'             => $this->getProductNo(),
            'product_variant'        => $this->getProductVariant(),
            'product_name'           => $this->getProductName(),
            'service'                => $this->getService(),
            'amount'                 => $this->getAmount(),
            'cancel'                 => $this->getCancel(),
            'cash_receipt_amount'    => $this->getCashRecieptAmount(),
            'statistical_value_line' => $this->getStatisticalValueLine()
        ];
    }

    /**
     * Получить объект в виде массива
     * @return array
     */
    public function toArray() {
        $vars = get_object_vars ( $this );
        $array = array ();
        foreach ( $vars as $key => $value ) {
            $array [ltrim ( $key, '_' )] = $value;
        }
        return $array;
    }

}