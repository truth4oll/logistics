<?php

namespace Carriers\Iml\Model;

/**
 * Статусы в системе IML
 *
 * Class Status
 * @package Carriers\Iml\Model
 */
class Status extends AbstractModel
{

    /**
     * @var
     */
    private $Code;

    /**
     * @var
     */
    private $Name;

    /**
     * @var
     */
    private $Description;

    /**
     * @var
     */
    private $StatusType;

    /**
     * @var
     */
    private $StatusTypeDescription;


    /**
     * @param $Code
     */
    public function setCode($Code)
    {
        $this->Code = $Code;
    }


    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->Code;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * @param mixed $Name
     */
    public function setName($Name)
    {
        $this->Name = $Name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param mixed $Description
     */
    public function setDescription($Description)
    {
        $this->Description = $Description;
    }

    /**
     * @return mixed
     */
    public function getStatusType()
    {
        return $this->StatusType;
    }

    /**
     * @param mixed $StatusType
     */
    public function setStatusType($StatusType)
    {
        $this->StatusType = $StatusType;
    }

    /**
     * @return mixed
     */
    public function getStatusTypeDescription()
    {
        return $this->StatusTypeDescription;
    }

    /**
     * @param mixed $StatusTypeDescription
     */
    public function setStatusTypeDescription($StatusTypeDescription)
    {
        $this->StatusTypeDescription = $StatusTypeDescription;
    }


}