<?php

namespace Carriers\Iml\Model;

use Carriers\Iml\Model\Query\GetPriceQuery;


/**
 * DTO цены полученной из API
 * @see GetPriceQuery
 * @see \Carriers\Iml\Api\Price
 *
 * Class Price
 * @package Carriers\Iml\Model
 */
class Price extends AbstractModel
{
    private $Price;

    /**
     * @param mixed $Price
     */
    public function setPrice($Price)
    {
        $this->Price = $Price;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->Price;
    }


}